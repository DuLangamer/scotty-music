const Discord = require('discord.js');
var DotJson = require('dot-json');

function trusted(x) {
  var DotJson = require('dot-json');
  var database = new DotJson('../blocked.json');
  var trustedMembers =  database.get('trusted');
  for (y = 0; y < trustedMembers.length; y++) {
    if (trustedMembers[y] == x) {return true}
  } return false
}
module.exports = {
    name: 'blacklist',
    aliases: [],
    category: 'Mods',
    utilisation: '{prefix}blacklist',

  execute(client, message, args, track) {
    if(trusted(message.author.id) == true) {
    var geblokkerdePersonen = new DotJson('../blocked.json');
    var blackList =  geblokkerdePersonen.get('blocked')
    var members = [];
    for (y = 0; y < blackList.length; y++) {
      members.push('<@!' + blackList[y] + '>')
    }
    var lijst = members.join('\n')
    if (lijst == '') {lijst = 'Geen leden verbannen'}
    message.channel.send({
        embed: {
            color: '#00BFFF',
            author: { name: 'Blacklist' },
            fields: [
                { name: 'Spelers zonder permissies:', value: lijst, inline:true },
            ],
            timestamp: new Date(),
        },
    });
  } else {return message.channel.send({
                  embed: {
                      color: '#e71837',
                      author: { name: 'Geen rechten' },
                      timestamp: new Date(),
                      description: `Je hebt de rechten niet om **/blacklist** uit te mogen voeren`,
                  }})};

  }}
