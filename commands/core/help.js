const Discord = require('discord.js');
var DotJson = require('dot-json');

function trusted(x) {
  var DotJson = require('dot-json');
  var database = new DotJson('../blocked.json');
  var trustedMembers =  database.get('trusted');
  for (y = 0; y < trustedMembers.length; y++) {
    if (trustedMembers[y] == x) {return true}
  } return false
}

module.exports = {
    name: 'help',
    aliases: ['h'],
    category: 'Music',
    utilisation: '{prefix}help <command name>',

    execute(client, message, args) {
        if (!args[0]) {
            const infos = message.client.commands.filter(x => x.category == 'Infos').map((x) => '`' + x.name + '`').join(', ');
            const music = message.client.commands.filter(x => x.category == 'Music').map((x) => '`' + x.name + '`').join(', ');
            const leiding = message.client.commands.filter(x => x.category == 'Leiding').map((x) => '`' + x.name + '`').join(', ');
            const mods = message.client.commands.filter(x => x.category == 'Mods').map((x) => '`' + x.name + '`').join(', ');

          if(trusted(message.author.id) == true) {
              return message.channel.send({
                  embed: {
                      color: '#00A551',
                      author: { name: 'Help menu' },
                      fields: [
                          { name: 'Scouts', value: music },
                          { name: 'Leiding', value: leiding },
                          { name: 'Mods', value: mods },
                        ],
                        timestamp: new Date(),
                        description: `Om hulp te krijgen bij een commando typ: **/help [commando]**`,
                      },
            })}

          if(message.member.roles.cache.some(r=>["Leiding", "Staf", "Developer"].includes(r.name)) ) {
              // has one of the roles
                return message.channel.send({
                    embed: {
                        color: '#00A551',
                        author: { name: 'Help menu' },
                        fields: [
                            { name: 'Scouts', value: music },
                            { name: 'Leiding', value: leiding },
                        ],
                        timestamp: new Date(),
                        description: `Om hulp te krijgen bij een commando typ: **/help [commando]**`,
                    },
                });
            }
              else {
                return message.channel.send({
                    embed: {
                        color: '#00A551',
                        author: { name: 'Help menu' },
                        fields: [
                            { name: 'Scouts', value: music },
                        ],
                        timestamp: new Date(),
                        description: `Om hulp te krijgen bij een commando typ: **/help [commando]**`,
                    },
                })};
        } else {
            const command = message.client.commands.get(args.join(" ").toLowerCase()) || message.client.commands.find(x => x.aliases && x.aliases.includes(args.join(" ").toLowerCase()));

            const EmbedNotKnown = new Discord.MessageEmbed()
                .setColor('#e71837')
            	.setTitle(`Dit is geen commando`)
            	.setDescription(`Bekijk alle mogelijke opdrachten met **/help** en voer die als volgt in voor meer informatie: **/help [commando]**`)
            	.setURL('https://minecraft.scouting.nl/')
            	.setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
            	.setTimestamp();
            if (!command) return message.channel.send(EmbedNotKnown);

            message.channel.send({
                embed: {
                    color: '#00A551',
                    author: { name: 'Help menu' },
                    fields: [
                        { name: 'Naam', value: command.name, inline: true },
                        { name: 'Categorie', value: command.category, inline: true },
                        { name: 'Aliase(s)', value: command.aliases.length < 1 ? 'None' : command.aliases.join(', '), inline: true },
                        { name: 'Gebruik', value: command.utilisation.replace('{prefix}', client.config.discord.prefix), inline: true },
                    ],
                    timestamp: new Date(),
                    description: 'Vind informatie over de aangegeven opdracht.\nNodige argumenten `[]`, optionele argumenten `<>`.',
                }
            });
        };
    },
};
