const Discord = require('discord.js')
module.exports = {
    name: 'loop',
    aliases: ['lp', 'repeat'],
    category: 'Music',
    utilisation: '{prefix}loop',

    execute(client, message, args) {
        const EmbedNotInChannel = new Discord.MessageEmbed()
            .setColor('#e71837')
            .setTitle(`Je zit niet in een spraakkanaal`)
            .setDescription(`Als je dit commando uit wil voeren moet je **${message.guild.me.voice.channel}** joinen`)
            .setURL('https://minecraft.scouting.nl/')
            .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
            .setTimestamp();
        if (!message.member.voice.channel) return message.channel.send(EmbedNotInChannel);


        const EmbedNoSongInQueue = new Discord.MessageEmbed()
            .setColor('#fc9303')
            .setTitle(`De wachtrij is leeg!`)
            .setDescription(`Er zitten nog geen liedjes in de wachtrij. Voeg liedjes toe met
                **/play [nummer]**`)
            .setURL('https://minecraft.scouting.nl/')
            .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
            .setTimestamp();
        if (!client.player.getQueue(message)) return message.channel.send(EmbedNoSongInQueue);


        const EmbedNotInSameChannel = new Discord.MessageEmbed()
            .setColor('#e71837')
            .setTitle(`Je zit niet in het goede spraakkanaal`)
            .setDescription(`Als je de muziek wil herhalen moet je in het volgende spraakkanaal zitten: **${message.guild.voice.channel.name}**`)
            .setURL('https://minecraft.scouting.nl/')
            .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
            .setTimestamp();
        if (message.guild.me.voice.channel && message.member.voice.channel.id !== message.guild.me.voice.channel.id) return message.channel.send(EmbedNotInSameChannel);


        const EmbedDisabled = new Discord.MessageEmbed()
            .setColor('#00A551')
            .setTitle(`Herhalen uitgeschakeld`)
            .setDescription(`Ik herhaal de muziek van de wachtlijst niet meer`)
            .setURL('https://minecraft.scouting.nl/')
            .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
            .setTimestamp();
        const EmbedEnabled = new Discord.MessageEmbed()
            .setColor('#00A551')
            .setTitle(`Herhalen ingeschakeld`)
            .setDescription(`Ik herhaal nu de wachtlijst tot dat iemand **herhalen** uitschakeld met **/loop**`)
            .setURL('https://minecraft.scouting.nl/')
            .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
            .setTimestamp();


        if (args.join(" ").toLowerCase() === 'queue') {
            if (client.player.getQueue(message).loopMode) {
                client.player.setLoopMode(message, false);
                return message.channel.send(EmbedDisabled);
            } else {
                client.player.setLoopMode(message, true);
                return message.channel.send(EmbedEnabled);
            };
        } else {
            if (client.player.getQueue(message).repeatMode) {
                client.player.setRepeatMode(message, false);
                return message.channel.send(EmbedDisabled);
            } else {
                client.player.setRepeatMode(message, true);
                return message.channel.send(EmbedEnabled);
            };
        };
    },
};