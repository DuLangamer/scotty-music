const Discord = require('discord.js')
module.exports = {
    name: 'volume',
    aliases: ['vol'],
    category: 'Music',
    utilisation: '{prefix}volume [50-100]',

    execute(client, message, args, track) {
        const EmbedNotInChannel = new Discord.MessageEmbed()
                    .setColor('#e71837')
                	.setTitle(`Je zit niet in een spraakkanaal`)
                	.setDescription(`Als je een liedje wil overslaan moet je een spraakkanaal joinen`)
                	.setURL('https://minecraft.scouting.nl/')
                	.setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
                	.setTimestamp();
        if (!message.member.voice.channel) return message.channel.send(EmbedNotInChannel);


        const EmbedNoMusicPlaying = new Discord.MessageEmbed()
                    .setColor('#fc9303')
                	.setTitle(`Er speelt geen muziek`)
                	.setDescription(`Je kan het volume niet aanpassen als er geen muziek speelt.
                	   Voeg nieuwe liedjes toe met: **/play [nummer**]`)
                	.setURL('https://minecraft.scouting.nl/')
                	.setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
                	.setTimestamp();
        if (!client.player.getQueue(message)) return message.channel.send(EmbedNoMusicPlaying);


        const EmbedNotInSameChannel = new Discord.MessageEmbed()
                    .setColor('#e71837')
                	.setTitle(`Je zit niet in het goede spraakkanaal`)
                	.setDescription(`Als je een liedje wil overslaan moet je in **${message.member.voice.channel.name}** zitten`)
                	.setURL('https://minecraft.scouting.nl/')
                	.setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
                	.setTimestamp();
        if (message.guild.me.voice.channel && message.member.voice.channel.id !== message.guild.me.voice.channel.id) return message.channel.send(EmbedNotInSameChannel);


        const EmbedValidNumber = new Discord.MessageEmbed()
                    .setColor('#fc9303')
                	.setTitle(`Dit is geen getal`)
                	.setDescription(`Voer een getal in, niks anders. Doe: **/volume [50-100]**`)
                	.setURL('https://minecraft.scouting.nl/')
                	.setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
                	.setTimestamp();
        if (!args[0] || isNaN(args[0]) || args[0] === 'Infinity') return message.channel.send(EmbedValidNumber);


        const EmbedValidNumber2 = new Discord.MessageEmbed()
                    .setColor('#fc9303')
                	.setTitle(`Dit getal is niet toegestaan`)
                	.setDescription(`Alleen volumes tussen **50** en **100** zijn toegestaan`)
                	.setURL('https://minecraft.scouting.nl/')
                	.setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
                	.setTimestamp();
        if (Math.round(parseInt(args[0])) < 50 || Math.round(parseInt(args[0])) > 100) return message.channel.send(EmbedValidNumber2);


        client.player.setVolume(message, parseInt(args[0]));
        const EmbedVolume = new Discord.MessageEmbed()
                    .setColor('#00A551')
                	.setTitle(`Volume is aangepast`)
                	.setDescription(`Het volume is nu **${parseInt(args[0])}%**`)
                	.setURL('https://minecraft.scouting.nl/')
                	.setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
                	.setTimestamp();
        message.channel.send(EmbedVolume);
    },
};
