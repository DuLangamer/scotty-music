const Discord = require('discord.js');
var DotJson = require('dot-json');
var geblokkerdePersonen = new DotJson('../blocked.json')
module.exports = {
    name: 'play',
    aliases: ['p'],
    category: 'Music',
    utilisation: '{prefix}play [name/URL]',

    execute(client, message, args, track) {
       const EmbedGeenToegang = new Discord.MessageEmbed()
        .setColor('#e71837')
        .setTitle(`Je toestemming voor dit commando is afgepakt`)
        .setDescription(`Je mag dit commando niet meer gebruiken omdat deze permissies zijn afgepakt door een van de leiding`)
        .setURL('https://minecraft.scouting.nl/')
        .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
        .setTimestamp();
      function checkId(x) {
        var arrayId = geblokkerdePersonen.get('blocked');
        for (y = 0; y < arrayId.length; y++) {
          if (x == arrayId[y]) {return true}
        }
        return false
      }
      if(checkId(message.author.id)) {message.channel.send(EmbedGeenToegang)
        return console.log(message.author.id + ' heeft geprobeerd om een commando te gebruiken')}
      else {
        const voiceChannelID = message.member.voice.channelID;
          if (voiceChannelID === '695964540244066365') {
            const EmbedAFKChannel = new Discord.MessageEmbed()
              .setColor('#e71837')
              .setTitle(`Je zit in het AFK-kanaal`)
              .setDescription(`Je kan hier geen liedjes afspelen. Stap in een ander kanaal om muziek te kunnen luisteren`)
              .setURL('https://minecraft.scouting.nl/')
              .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
              .setTimestamp();
            message.channel.send(EmbedAFKChannel);
          } else {
            const EmbedNotInChannel = new Discord.MessageEmbed()
              .setColor('#e71837')
            	.setTitle(`Je zit niet in een spraakkanaal`)
            	.setDescription(`Als je een liedje wil spelen moet je een spraakkanaal joinen`)
        	    .setURL('https://minecraft.scouting.nl/')
        	    .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
        	    .setTimestamp();
            if (!message.member.voice.channel) return message.channel.send(EmbedNotInChannel);


            const EmbedNoTitle = new Discord.MessageEmbed()
              .setColor('#fc9303')
        	    .setTitle(`Geen nummer opgegeven`)
        	    .setDescription(`Je hebt geen muzieknummer opgegeven. Start muziek met: **/play [nummer]**`)
        	    .setURL('https://minecraft.scouting.nl/')
        	    .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
        	    .setTimestamp();
            if (!args[0]) return message.channel.send(EmbedNoTitle);


            const EmbedNotInSameChannel = new Discord.MessageEmbed()
              .setColor('#e71837')
              .setTitle(`Je zit niet in het goede spraakkanaal`)
              .setDescription(`Als je een liedje wil afspelen moet je in het volgende spraakkanaal zitten: **${message.guild.me.voice.channel}**`)
              .setURL('https://minecraft.scouting.nl/')
              .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
              .setTimestamp();
            if (message.guild.me.voice.channel && message.member.voice.channel.id !== message.guild.me.voice.channel.id) return message.channel.send(EmbedNotInSameChannel);

            client.player.play(message, args.join(" "), { firstResult: true })};
    }},
};
