const Discord = require('discord.js');
module.exports =  {
    name: 'queue',
    aliases: [],
    category: 'Music',
    utilisation: '{prefix}queue',


    execute(client, message, track) {
        const EmbedNotInChannel = new Discord.MessageEmbed()
            .setColor('#e71837')
        	  .setTitle(`Je zit niet in een spraakkanaal`)
        	  .setDescription(`Als je de wachtrij wil bekijken moet je een spraakkanaal joinen en een of meerdere liedjes starten met **/play [nummer]**`)
        	  .setURL('https://minecraft.scouting.nl/')
        	  .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
        	  .setTimestamp();
        if (!message.member.voice.channel) return message.channel.send(EmbedNotInChannel);


        const EmbedNoSongInQueue = new Discord.MessageEmbed()
            .setColor('#fc9303')
            .setTitle(`De wachtrij is leeg!`)
            .setDescription(`Er zitten nog geen liedjes in de wachtrij. Voeg liedjes toe met
                **/play [nummer]**`)
            .setURL('https://minecraft.scouting.nl/')
            .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
            .setTimestamp();
        if (!client.player.getQueue(message)) return message.channel.send(EmbedNoSongInQueue);


        const EmbedNotInSameChannel = new Discord.MessageEmbed()
            .setColor('#e71837')
            .setTitle(`Je zit niet in het goede spraakkanaal`)
            .setDescription(`Als je de wachtrij wil bekijken moet je in het volgende spraakkanaal zitten: **${message.guild.voice.channel.name}**`)
            .setURL('https://minecraft.scouting.nl/')
            .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
            .setTimestamp();
        if (message.guild.me.voice.channel && message.member.voice.channel.id !== message.guild.me.voice.channel.id) return message.channel.send(EmbedNotInSameChannel);


        const queue = client.player.getQueue(message);
        const EmbedQueue = new Discord.MessageEmbed()
            .setColor('#00A551')
            .setTitle(`Wachtrij`)
            .addFields({
                name: `De wachtrij van ${client.user.username}`,
                value: (queue.tracks.map((track, i) => {
                           return `
                           **#${i + 1}** - ${track.title} | ${track.author} (toegevoegd door : ${track.requestedBy.username})`
                       }).slice(0, 5).join('\n') + `\n\n${queue.tracks.length > 5 ? `en **${queue.tracks.length - 5}** ander(e) liedje(s)...` : `**${queue.tracks.length}** liedje(s) in de wachtrij...`}`)
            })
            .setURL('https://minecraft.scouting.nl/')
            .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
            .setTimestamp();
        message.channel.send(EmbedQueue)

    },
};
