const Discord = require('discord.js');
module.exports = {
    name: 'resume',
    aliases: [],
    category: 'Music',
    utilisation: '{prefix}resume',

    execute(client, message) {
        const EmbedNotInChannel = new Discord.MessageEmbed()
            .setColor('#e71837')
        	.setTitle(`Je zit niet in een spraakkanaal`)
        	.setDescription(`Join eerst een spraakkanaal en start een liedje met **/play [nummer]**`)
        	.setURL('https://minecraft.scouting.nl/')
        	.setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
        	.setTimestamp();
        if (!message.member.voice.channel) return message.channel.send(EmbedNotInChannel);


        const EmbedNoSongInQueue = new Discord.MessageEmbed()
            .setColor('#fc9303')
            .setTitle(`De wachtrij is leeg!`)
            .setDescription(`Er zitten nog geen liedjes in de wachtrij. Voeg liedjes toe met
                **/play [nummer]**`)
            .setURL('https://minecraft.scouting.nl/')
            .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
            .setTimestamp();
        if (!client.player.getQueue(message)) return message.channel.send(EmbedNoSongInQueue);


        const EmbedNotInSameChannel = new Discord.MessageEmbed()
            .setColor('#e71837')
            .setTitle(`Je zit niet in het goede spraakkanaal`)
            .setDescription(`Als je de muziek wil hervatten moet je in het volgende spraakkanaal zitten: **${message.guild.voice.channel.name}**`)
            .setURL('https://minecraft.scouting.nl/')
            .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
            .setTimestamp();
        if (message.guild.me.voice.channel && message.member.voice.channel.id !== message.guild.me.voice.channel.id) return message.channel.send(EmbedNotInSameChannel);


        const EmbedAlreadyPlaying = new Discord.MessageEmbed()
            .setColor('#e71837')
            .setTitle(`De muziek speelt al`)
            .setDescription(`De muziek is niet gepauzeerd. je kan de muziek pauzeren met **/pause**`)
            .setURL('https://minecraft.scouting.nl/')
            .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
            .setTimestamp();
        if (!client.player.getQueue(message).paused) return message.channel.send(EmbedAlreadyPlaying);


        client.player.resume(message);
        const EmbedResume = new Discord.MessageEmbed()
            .setColor('#00A551')
            .setTitle(`De muziek speelt weer verder`)
            .setDescription(`De muziek is niet gepauzeerd. je kan de muziek pauzeren met **/pause**`)
            .setURL('https://minecraft.scouting.nl/')
            .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
            .setTimestamp();
        message.channel.send(EmbedResume);
    },
};