const Discord = require('discord.js');
module.exports = {
    name: 'join',
    aliases: [],
    category: 'Music',
    utilisation: '{prefix}join',

    execute(client, message, args, track) {
      const voiceChannelID = message.member.voice.channelID;
        if (voiceChannelID === '695964540244066365') {
          const EmbedAFKChannel = new Discord.MessageEmbed()
            .setColor('#e71837')
            .setTitle(`Je zit in het AFK-kanaal`)
            .setDescription(`Je kan me niet vragen om in het afk-kanaal te stappen. Stap in een ander kanaal om mij te roepen`)
            .setURL('https://minecraft.scouting.nl/')
            .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
            .setTimestamp();
            message.channel.send(EmbedAFKChannel);
        } else {
          const EmbedNotInChannel = new Discord.MessageEmbed()
            .setColor('#e71837')
        	  .setTitle(`Je zit niet in een spraakkanaal`)
        	  .setDescription(`Als je mij wil laten joinen moet je eerst in een spraakkanaal stappen`)
        	  .setURL('https://minecraft.scouting.nl/')
        	  .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
        	  .setTimestamp();
          if (!message.member.voice.channel) return message.channel.send(EmbedNotInChannel);

          const EmbedNotInSameChannel = new Discord.MessageEmbed()
            .setColor('#e71837')
            .setTitle(`Ik zit al in een spraakkanaal`)
            .setDescription(`Je kan dit commando niet gebruiken als ik al actief ben in een spraakkanaal`)
            .setURL('https://minecraft.scouting.nl/')
            .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
            .setTimestamp();
          if (message.guild.me.voice.channel && message.member.voice.channel.id !== message.guild.me.voice.channel.id) return message.channel.send(EmbedNotInSameChannel);

          const EmbedJoined = new Discord.MessageEmbed()
            .setColor('#00A551')
            .setTitle(`Gejoined`)
            .setDescription(`Ik ben bij je in het kanaal gestapt`)
            .setURL('https://minecraft.scouting.nl/')
            .setThumbnail('https://minecraft.scouting.nl/wp-content/uploads/2020/04/Logo_Square.png')
            .setTimestamp();
          message.channel.send(EmbedJoined);
          message.member.voice.channel.join(" ")
}}};
